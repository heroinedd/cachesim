from simulator.bus import *
from simulator.utils import *
import logging


class State(Enum):
    Shared = 1
    Modified = 2
    Invalid = 3


StateTransformer = {
    State.Shared: {
        Signal.RR: State.Shared,
        Signal.WR: State.Invalid,
        Signal.RL: State.Shared,
        Signal.WL: State.Modified,
    },
    State.Modified: {
        Signal.RR: State.Shared,
        Signal.WR: State.Invalid,
        Signal.RL: State.Modified,
        Signal.WL: State.Modified,
    },
    State.Invalid: {
        Signal.RR: State.Invalid,
        Signal.WR: State.Invalid,
        Signal.RL: State.Shared,
        Signal.WL: State.Modified,
    },
}


class Block:
    def __init__(self, n_byte: int):
        self.tag = -1
        self.state = State.Invalid
        self.data = [0] * n_byte
        self.timestamp = -1

    def change_state(self, signal: Signal):
        old_state = self.state
        self.state = StateTransformer[self.state][signal]
        # logging.info('{} -> {}'.format(old_state, self.state))

    def __str__(self):
        return f'tag: {self.tag}\tstate: {self.state}\tdata: {self.data}'


class BlockSet:
    def __init__(self, no: int, n_block: int, n_byte: int):
        self.no = no
        self.blocks = [Block(n_byte) for _ in range(n_block)]

    def search(self, tag: int) -> Block:
        for i, block in enumerate(self.blocks):
            if block.tag == tag:
                return block

    def find(self) -> Block:
        """
        :return: an empty block or the least recently used block
        """
        oldest = -1
        for i, block in enumerate(self.blocks):
            # empty block
            if block.tag == -1:
                return block
            # LRU replacement policy
            if oldest == -1 or block.timestamp < self.blocks[oldest].timestamp:
                oldest = i
        return self.blocks[oldest]


class Cache:
    def __init__(self, no: int, n_set: int, n_block: int, n_byte: int):
        self.no = no
        self.n_set = n_set
        self.n_block = n_block
        self.n_byte = n_byte

        self.sets = [BlockSet(i, n_block, n_byte) for i in range(self.n_set)]

        self.bus = None

    def _index(self, address):
        """
        mapping memory address into cache address
        :param address: memory address
        :return: cache_set_index, tag, block_offset
        """
        memory_block_index = address // self.n_byte
        block_offset = address % self.n_byte
        cache_set_index = memory_block_index % self.n_set
        tag = address // (self.n_set * self.n_byte)

        return cache_set_index, tag, block_offset

    # helper function for logging
    def log_state(self, address):
        cache_set_index, tag, block_offset = self._index(address)
        block = self.sets[cache_set_index].search(tag)
        if block is not None:
            logging.info('[CACHE {}] {}'.format(self.no, block.state))
        else:
            logging.info('[CACHE {}] miss'.format(self.no))

    # read data from cache
    def read(self, address):
        dat = self._helper(address, None)
        logging.debug('[CACHE {}] read {} get {}'.format(self.no, address, dat))
        self.bus.log_state(address)
        return dat

    # write data to cache
    def write(self, address, value):
        self._helper(address, value)
        logging.debug('[CACHE {}] write {} to {}'.format(self.no, value, address))
        self.bus.log_state(address)

    def _helper(self, address, value):
        signal = Signal.RL if value is None else Signal.WL
        dat = self.inform(address, signal)

        cache_set_index, tag, block_offset = self._index(address)
        block = self.sets[cache_set_index].search(tag)

        # cache miss
        if block is None:
            block = self._find_block(cache_set_index, address)

        self._write_block(block, dat, address)
        block.timestamp = clock.time
        block.change_state(signal)
        if value is None:
            return block.data[block_offset]
        else:
            block.data[block_offset] = value

    def _find_block(self, cache_set_index: int, address) -> Block:
        block = self.sets[cache_set_index].find()
        if block.tag != -1:
            self._write_memory(cache_set_index, block)
        block.tag = address // (self.n_set * self.n_byte)
        block.state = State.Invalid
        return block

    def _write_block(self, block: Block, dat, address):
        if dat is not None:
            block.data = dat
        elif block.state == State.Invalid:
            block.data = self._read_memory(address)

    def _read_memory(self, address):
        address = address - address % self.n_byte
        return self.bus.read_memory(address, self.n_byte)

    def _write_memory(self, cache_set_index: int, block: Block):
        address = (block.tag * self.n_set + cache_set_index) * self.n_byte
        self.bus.write_memory(address, block.data)

    # listen to signals from bus
    def listen(self, address, signal: Signal):
        cache_set_index, tag, block_offset = self._index(address)
        block = self.sets[cache_set_index].search(tag)
        if block is not None:
            state = block.state
            block.change_state(signal)
            if state == State.Modified:
                self._write_memory(cache_set_index, block)
                return block.data

    # let the bus know we read/write here
    def inform(self, address, signal: Signal):
        return self.bus.listen(self.no, address, signal)
