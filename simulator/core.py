import logging
import random


class Core:
    def __init__(self, no: int):
        self.no = no
        self.cache = None

    def execute(self, trace: str):
        with open(trace) as f:
            for line in f.readlines():
                logging.info('[CORE {}] {}'.format(self.no, line.strip('\n')))
                tmp = line.split(' ')
                op = int(tmp[0])  # get the operation, 0 for read and 1 for write
                address = int(tmp[1], 16)  # get the memory address
                if op == 0:
                    self.read(address)
                elif op == 1:
                    self.write(address, random.randint(1025, 2048))
                yield False
        while True:
            yield True

    def read(self, address):
        return self.cache.read(address)

    def write(self, address, value):
        self.cache.write(address, value)
