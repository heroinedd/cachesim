import logging
import datetime


stream_handler = logging.StreamHandler()
file_handler = logging.FileHandler('simulator/logs/{}.log'.format(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')))
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
stream_handler.setFormatter(formatter)
file_handler.setFormatter(formatter)
logging.basicConfig(level=logging.DEBUG, handlers=[stream_handler, file_handler])


class Clock:
    def __init__(self):
        self.time = 0

    def count(self):
        self.time += 1


clock = Clock()
