from typing import List

from simulator.core import *
from simulator.cache import *
from simulator.memory import *
from simulator.utils import *


class CPU:
    def __init__(self, n_core: int, n_set: int, n_block: int, n_byte: int):
        # initialize every module
        self.bus = Bus()
        self.memory = Memory(1024)
        self.caches = [Cache(i, n_set, n_block, n_byte) for i in range(n_core)]
        self.cores = [Core(i) for i in range(n_core)]

        # connect memory and bus
        self.bus.memory = self.memory

        # connect caches and bus
        self.bus.caches = self.caches
        for cache in self.caches:
            cache.bus = self.bus

        # connect caches and cores
        for i, core in enumerate(self.cores):
            core.cache = self.caches[i]

    def run(self, traces: List[str]):
        executors = [core.execute(traces[i]) for i, core in enumerate(self.cores)]
        finish = False
        while not finish:
            finish = True
            for executor in executors:
                finish = next(executor) and finish
            clock.count()
