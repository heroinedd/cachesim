class Memory:
    def __init__(self, size: int):
        self.data = [i for i in range(size)]

    def read(self, address, length=1):
        return self.data[address: address + length]

    def write(self, address, value):
        self.data[address: address + len(value)] = value
