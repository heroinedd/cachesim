from enum import Enum


class Signal(Enum):
    RR = 1
    WR = 2
    RL = 3
    WL = 4


SignalTransformer = {
    Signal.RL: Signal.RR,
    Signal.WL: Signal.WR,
}


class Bus:
    def __init__(self):
        self.memory = None
        self.caches = []

    def listen(self, no: int, address, signal: Signal):
        """
        :param no: cache number
        :param address: memory address
        :param signal: Signal.RL/Signal.WL corresponding to read/write
        :return: None
        If one cache reads/writes a cached variable, it will send Signal.RL/Signal.WL to bus.
        Then, bus will inform all other caches with Signal.RR/Signal.WR.
        """
        signal = SignalTransformer[signal]
        dat = None
        for cache in self.caches:
            if cache.no != no:
                tmp = cache.listen(address, signal)
                if tmp is not None:
                    dat = tmp
        return dat

    def read_memory(self, address, length):
        return self.memory.read(address, length)

    def write_memory(self, address, value):
        self.memory.write(address, value)

    # helper function for logging
    def log_state(self, address):
        for cache in self.caches:
            cache.log_state(address)
