from simulator.cpu import *


if __name__ == '__main__':
    cpu = CPU(n_core=4, n_set=2, n_block=2, n_byte=64)
    traces = [
        'simulator/traces/trace0.txt',
        'simulator/traces/trace1.txt',
        'simulator/traces/trace2.txt',
        'simulator/traces/trace3.txt',
    ]
    cpu.run(traces)
